#ifndef _TESTS_H_
#define _TESTS_H_
#include "mem.h"
#include "mem_internals.h"

void test1(struct block_header *block);
void test2(struct block_header *block);
void test3(struct block_header *block);
void test4(struct block_header *block);
void test5(struct block_header *block);
void run();

#endif

#include "tests.h"
#include "mem_debug.h"
#include "util.h"

static struct block_header* get_block(void* data) {
    return (struct block_header*) ((uint8_t*) data - offsetof(struct block_header, contents));
}


void test1(struct block_header *block) {
    debug("Test1: выделение памяти 1 раз и удаление\n");
    void *data = _malloc(1000);
    if (data == NULL) {
        err("Error: malloc вернул null");
    }
    debug_heap(stdout, block);
    if (block->is_free != false || block->capacity.bytes != 1000) {
        err("Error: is_free или capacity выставились некорректно");
    }
    _free(data);
    debug_heap(stdout, block);
    debug("Test1 successes!\n\n");
}


void test2(struct block_header *block) {
    debug("Test2: выделение памяти 2 раза и удаление 1 раз\n");
    void *data1 = _malloc(1000);
    void *data2 = _malloc(1000);
    if (data1 == NULL || data2 == NULL) {
        err("Error: malloc вернул null");
    }
    debug_heap(stdout, block);
    _free(data2);
    debug_heap(stdout, block);

    struct block_header *data1_block = get_block(data1);
    struct block_header *data2_block = get_block(data2);
    if (data2_block->is_free == false) {
        err("Error: блок 2 не очищен");
    }
    if (data1_block->is_free == true) {
        err("Error: блок 1 очищен");
    }
    debug("Test2 successes!\n\n");
    _free(data1);
}


void test3 (struct block_header *block) {
    debug("Test3: выделение памяти 3 раза и удаление 2 раза\n");
    void *data1 = _malloc(1000);
    void *data2 = _malloc(1000);
    void *data3 = _malloc(1000);
    if (data1 == NULL || data2 == NULL || data3 == NULL) {
        err("Error: malloc вернул null");
    }
    debug_heap(stdout, block);
    _free(data2);
    debug_heap(stdout, block);
    _free(data3);
    debug_heap(stdout, block);

    struct block_header *data1_header = get_block(data1);
    struct block_header *data2_header = get_block(data2);
    struct block_header *data3_header = get_block(data3);
    if (data2_header->is_free == false) {
        err("Error: блок 2 не очищен");
    }
    if (data3_header->is_free == false) {
        err("Error: блок 3 не очищен");
    }
    if (data1_header->is_free == true) {
        err("Error: блок 1 очищен");
    }
    debug("Test3 successes!\n\n");
    _free(data1);
}


void test4(struct block_header *block) {
    debug("Test4: изначального места в куче слишком мало, нужно расширить кучу\n");
    void *data1 = _malloc(10000);
    void *data2 = _malloc(10000);
    if (data1 == NULL || data2 == NULL) {
        err("Error: malloc вернул null");
    }
    debug_heap(stdout, block);

    struct block_header *data1_block = get_block(data1);
    struct block_header *data2_block = get_block(data2);
    if ((uint8_t*) data1_block->contents + data1_block->capacity.bytes != (uint8_t*) data2_block){
        err("Error: блок выделен не рядом");
    }
    debug("Test4 successes!\n\n");
    _free(data2);
    _free(data1);
}


void test5(struct block_header *block) {
    debug("Test5: изначального места в куче слишком мало, надо выделить в другом месте\n");
    void *data1 = _malloc(10000*3);
    if (data1 == NULL) {
        err("Блок равен NULL");
    }

    struct block_header *start = block;
    while (start->next != NULL) start = start->next;
    map_pages((uint8_t *) start + size_from_capacity(start->capacity).bytes, 1000, MAP_FIXED_NOREPLACE);
    void *data2 = _malloc(10000*30);
    debug_heap(stdout, block);

    struct block_header *data2_block = get_block(data2);
    if (data2_block == start) {
        err("Error: блок выделился рядом");
    }
    debug("Test5 successes!\n\n");
    _free(data2);
    _free(data1);
}


void run() {
    debug("Инициализируем кучу с размером 10000...\n");
    void* heap = heap_init(10000);

    if (heap == NULL) {
        err("Error: невозможно инициализировать кучу");
    } else {
        debug("Куча успешно инициализирована!\n\n");
        struct block_header* block = (struct block_header*) heap;

        test1(block);
        debug("\nКуча после Test1:\n");
        debug_heap(stdout, block);
        test2(block);
        debug("\nКуча после Test2:\n");
        debug_heap(stdout, block);
        test3(block);
        debug("\nКуча после Test3:\n");
        debug_heap(stdout, block);
        test4(block);
        debug("\nКуча после Test4:\n");
        debug_heap(stdout, block);
        test5(block);
        debug("\nКуча после Test5:\n");
        debug_heap(stdout, block);
        debug("\nУспех, тесты пройдены)\n");
    }
}
